# Test assignment for Sprout

# How to run
0. Install [poetry](https://python-poetry.org/docs/#installation)
0. Run `poetry install --no-dev`
0. Activate newly created virtualenv
0. Run `uvicorn main:app`


# How to test
0. Run `poetry install`
0. Activate newly created virtualenv
0. Run `pytest --cov src/`
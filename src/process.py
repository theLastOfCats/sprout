from typing import Optional

from fastapi import HTTPException

from src.models import H, InputModel, OutputModel


def custom_bool_rules(a, b, c) -> Optional[H]:
    """
    Extended set of rules for boolean fields
    """

    # A && B && !C => H = T
    if a and b and not c:
        return H.T
    # A && !B && C => H = M
    elif a and not b and c:
        return H.M


def base_bool_rules(a, b, c) -> Optional[H]:
    """
    Basic set of rules for boolean fields
    """

    # A && B && !C => H = M
    if a and b and not c:
        return H.M
    # A && B && C => H = P
    elif a and b and c:
        return H.P
    # !A && B && C => H = T
    elif not a and b and c:
        return H.T


def custom_numeric_rules(h: H, d: float, e: int, f: int) -> float:
    """
    Extended set of rules for numeric fields
    """

    if h == H.P:
        return 2 * d + (d * e / 100)
    elif h == H.M:
        return f + d + (d * e / 100)


def base_numeric_rules(h: H, d: float, e: int, f: int) -> float:
    """
    Basic set of rules for numeric fields
    """
    if h == H.M:
        return d + (d * e / 10)
    elif h == H.P:
        return d + (d * (e - f) / 25.5)
    elif h == H.T:
        return d - (d * f / 30)


def process_bool(a: bool, b: bool, c: bool) -> Optional[H]:
    if custom := custom_bool_rules(a, b, c):
        return custom
    
    return base_bool_rules(a, b, c)


def process_numeric(h: H, d: float, e: int, f: int) -> float:
    if custom := custom_numeric_rules(h, d, e, f):
        return custom
    
    return base_numeric_rules(h, d, e, f)


def process(data: InputModel) -> OutputModel:
    """
    Process input data
    """

    if h := process_bool(data.a, data.b, data.c):
        k = process_numeric(h, data.d, data.e, data.f)
        return OutputModel(h=h, k=k)

    raise HTTPException(status_code=400, detail="error")

    

    
    


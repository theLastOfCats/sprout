import uvicorn
from fastapi import FastAPI
from src.models import InputModel, OutputModel
from src.process import process

app = FastAPI()


@app.post("/calculate", response_model=OutputModel)
def calculate(data: InputModel):
    return process(data)


if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)

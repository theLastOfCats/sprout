import pytest
from fastapi import HTTPException

from src.models import H, InputModel, OutputModel
from src.process import (
    base_bool_rules, custom_bool_rules,
    base_numeric_rules, custom_numeric_rules,
    process_bool, process_numeric, process,
)


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ([True, True, True], None),
        ([False, False, False], None),

        ([True, False, False], None),
        ([False, True, False], None),
        ([False, False, True], None),

        ([False, True, True], None),
        ([True, True, False], H.T),
        ([True, False, True], H.M),
    ],
)
def test_custom_bool_rules(test_input, expected):
    assert custom_bool_rules(*test_input) == expected


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ([True, True, True], H.P),
        ([False, False, False], None),

        ([True, False, False], None),
        ([False, True, False], None),
        ([False, False, True], None),

        ([False, True, True], H.T),
        ([True, True, False], H.M),
        ([True, False, True], None),
    ],
)
def test_base_bool_rules(test_input, expected):
    assert base_bool_rules(*test_input) == expected


@pytest.mark.parametrize(
    "h, expected",
    [
        (H.M, 700.0),
        (H.P, 900.0),
        (H.T, None),
    ]
)
def test_custom_numeric_rules(h, expected):
    assert custom_numeric_rules(h, 300, 100, 100) == expected


@pytest.mark.parametrize(
    "h, expected",
    [
        (H.M, 3300.0),
        (H.P, 300.0),
        (H.T, -700.0),
    ]
)
def test_base_numeric_rules(h, expected):
    assert base_numeric_rules(h, 300, 100, 100) == expected


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ([True, True, True], H.P),
        ([False, False, False], None),

        ([True, False, False], None),
        ([False, True, False], None),
        ([False, False, True], None),

        ([False, True, True], H.T),
        ([True, True, False], H.T),
        ([True, False, True], H.M),
    ],
)
def test_process_bool(test_input, expected):
    assert process_bool(*test_input) == expected


@pytest.mark.parametrize(
    "h, expected",
    [
        (H.M, 700.0),
        (H.P, 900.0),
        (H.T, -700.0),
    ]
)
def test_process_numeric(h, expected):
    assert process_numeric(h, 300, 100, 100) == expected


@pytest.mark.parametrize(
    "data, expected",
    [
        (InputModel(a=True, b=True, c=True, d=300, e=100, f=100), OutputModel(h=H.P, k=900.0)),
        (InputModel(a=False, b=True, c=True, d=300, e=100, f=100), OutputModel(h=H.T, k=-700.0)),
        (InputModel(a=True, b=True, c=False, d=300, e=100, f=100), OutputModel(h=H.T, k=-700.0)),
        (InputModel(a=True, b=False, c=True, d=300, e=100, f=100), OutputModel(h=H.M, k=700.0)),
    ]
)
def test_process(data, expected):
    assert process(data) == expected


@pytest.mark.parametrize(
    "data",
    [
        InputModel(a=False, b=False, c=False, d=300, e=100, f=100),
        InputModel(a=True, b=False, c=False, d=300, e=100, f=100),
        InputModel(a=True, b=False, c=False, d=300, e=100, f=100),
        InputModel(a=True, b=False, c=False, d=300, e=100, f=100),
        InputModel(a=False, b=True, c=False, d=300, e=100, f=100),
        InputModel(a=False, b=False, c=True, d=300, e=100, f=100),
    ]
)
def test_incorrect_process(data):
    with pytest.raises(HTTPException):
        process(data)

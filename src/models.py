from enum import Enum
from pydantic import BaseModel


class H(Enum):
    M = "M"
    P = "P"
    T = "T"


class InputModel(BaseModel):
    a: bool
    b: bool
    c: bool
    d: float
    e: int
    f: int


class OutputModel(BaseModel):
    h: H
    k: float

